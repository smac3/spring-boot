/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.mocktest6.business.service;

import com.example.mocktest6.database.dto.HelpDTO;
import com.example.mocktest6.database.dto.StaffDTO;
import java.util.List;

/**
 *
 * @author black
 */
public interface StaffServiceInterface {
    public List<StaffDTO> getAll();
    public List<HelpDTO> getAllHelps();
}
