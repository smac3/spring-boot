/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.mocktest6.business.service;

import com.example.mocktest6.database.model.Help;
import java.util.List;

/**
 *
 * @author black
 */
public interface HelpServiceInterface {
    public List<Help> getAll();
     public Help update(Help help);
     public Help create(Help help);
     public int delete(int helpId);
}
