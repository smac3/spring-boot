/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.mocktest6.business.service.impl;

import com.example.mocktest6.business.service.StaffServiceInterface;
import com.example.mocktest6.database.dto.HelpDTO;
import com.example.mocktest6.database.dto.StaffDTO;
import com.example.mocktest6.database.model.Help;
import com.example.mocktest6.database.model.Staff;
import com.example.mocktest6.database.repository.HelpRepo;
import com.example.mocktest6.database.repository.StaffRepo;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author black
 */
@Service
public class StaffService implements StaffServiceInterface{

    @Autowired
    private StaffRepo staffRepo;

    @Autowired
    private HelpRepo helpRepo;

    /**
     *
     * @return
     */
    @Override
    public List<StaffDTO> getAll() {
        List<Staff> listAffStaff = staffRepo.findAll();
        List<StaffDTO> staffDTOs = new ArrayList<>();
        for (Staff staff : listAffStaff) {
            List<Help> helpListByStaffCode = helpRepo.findByCreatedUser(staff.getStaffCode());
            staff.setHelps(helpListByStaffCode);

            for (Help help : helpListByStaffCode) {
                StaffDTO staffDto = new StaffDTO();
                staffDto.setStaffCode(staff.getStaffCode());
                staffDto.setStaffName(staff.getStaffName());
                staffDto.setTel(staff.getStaffName());
                staffDto.setShopId(staff.getShopId());
                staffDto.setIdNo(staff.getIdNo());
                staffDto.setHelpId(help.getHelpId());
                staffDTOs.add(staffDto);
            }
        }
        return staffDTOs;
    }
    
    
    @Override
    public List<HelpDTO> getAllHelps() {
        List<Staff> staffs = staffRepo.findAll();
        List<HelpDTO> helpDtos = new ArrayList<>();
        for (Staff staff : staffs) {
            List<Help> helps = helpRepo.findByCreatedUser(staff.getStaffCode());
            for (Help help : helps) {
                HelpDTO helpDto = new HelpDTO();
                helpDto.setParentHelpId(help.getParentHelpId());
                helpDto.setShopId(staff.getShopId());
                helpDto.setStaffId(staff.getStaffId());
                helpDto.setStatus(help.getStatus());
                helpDto.setType(help.getType());
                helpDtos.add(helpDto);
            }
        }
        return helpDtos;
    }

}
