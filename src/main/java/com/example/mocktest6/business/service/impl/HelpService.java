package com.example.mocktest6.business.service.impl;

import com.example.mocktest6.business.service.HelpServiceInterface;
import com.example.mocktest6.database.model.Help;
import com.example.mocktest6.database.repository.HelpRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Service
public class HelpService implements HelpServiceInterface {
    @Autowired
    private HelpRepo helpRepo;

    

    
    @Override
    public List<Help> getAll() {
        List<Help> helps = helpRepo.findAll();
        return helps;
    }

    
    @Override
    public Help update(Help help) {
        Optional<Help> optionalHelp = helpRepo.findById(help.getHelpId());
        if (optionalHelp.isPresent()) {
            Help helpNew = optionalHelp.get();
            helpNew.setContent(help.getContent());
            helpNew.setCreatedUser(help.getCreatedUser());
            helpNew.setParentHelpId(help.getParentHelpId());
            helpNew.setHelpName(help.getHelpName());
            helpNew.setPosition(help.getPosition());
            helpNew.setStatus(help.getStatus());
            helpNew.setType(help.getType());
            return helpRepo.save(helpNew);
        }
        return null;
    }
    
    @Override
    public Help create(Help help) {
        return helpRepo.save(help);
    }

    @ExceptionHandler
    @Override
    public int delete(int helpId) {
            List<Integer> helpIds = helpRepo.selectHelpIdLevel(helpId);
            for (Integer helpIdSub : helpIds) {
                helpRepo.delete(helpIdSub);
            }
            return 1;
    }

        
    
}
