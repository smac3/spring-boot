/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.mocktest6.business.controller;

import com.example.mocktest6.database.dto.StaffDTO;
import java.util.List;

/**
 *
 * @author black
 */
public interface StaffControllerInterface {
    public List<StaffDTO> getAll();
}
