package com.example.mocktest6.business.controller.impl;

import com.example.mocktest6.business.controller.HelpControllerInterface;
import com.example.mocktest6.database.dto.HelpDTO;
import com.example.mocktest6.business.service.impl.HelpService;
import com.example.mocktest6.business.service.impl.StaffService;
import com.example.mocktest6.database.model.Help;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("/api/helps")
public class HelpController implements HelpControllerInterface{

    @Autowired
    private StaffService staffService;

    @Autowired
    private HelpService helpService;

    @GetMapping("/list")
    public List<HelpDTO> getAll() {
        List<HelpDTO> helpDtos = staffService.getAllHelps();
        return helpDtos;

    }

    @GetMapping("/delete")
    public Integer delete(@RequestParam("helpId") int helpId) {
        int error = helpService.delete(helpId);
        return 1;
    }
    
    @PostMapping("/add")
    public Help addHelp(@RequestBody Help help) {
        // save the product to the database or perform other operations
        return helpService.create(help);
    }
    
    @PostMapping("/update")
    public Help updateHelp(@RequestBody Help help) {
        // save the product to the database or perform other operations
        return helpService.update(help);
    }

}
