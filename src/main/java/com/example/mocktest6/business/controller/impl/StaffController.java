/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.mocktest6.business.controller.impl;

import com.example.mocktest6.business.controller.StaffControllerInterface;
import com.example.mocktest6.database.dto.StaffDTO;
import com.example.mocktest6.business.service.impl.StaffService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author black
 */
@RestController
@RequestMapping("/api/staffs")
public class StaffController implements StaffControllerInterface{

    @Autowired
    private StaffService staffService;

    /**
     *
     * @return
     */
    @GetMapping("/list")
    @Override
    public List<StaffDTO> getAll() {
        List<StaffDTO> staffDtos = staffService.getAll();
        return staffDtos;
    }
    

    
    

}
