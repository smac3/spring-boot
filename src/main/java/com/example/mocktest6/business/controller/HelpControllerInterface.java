/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.mocktest6.business.controller;

import com.example.mocktest6.database.dto.HelpDTO;
import com.example.mocktest6.database.model.Help;
import java.util.List;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author black
 */
public interface HelpControllerInterface {
    public List<HelpDTO> getAll();
    public Integer delete(@RequestParam("helpId") int helpId);
    public Help addHelp(@RequestBody Help help);
    public Help updateHelp(@RequestBody Help help);
}
