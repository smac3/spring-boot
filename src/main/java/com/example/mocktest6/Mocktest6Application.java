package com.example.mocktest6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mocktest6Application {

	public static void main(String[] args) {
		SpringApplication.run(Mocktest6Application.class, args);
	}

}
