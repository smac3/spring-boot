package com.example.mocktest6.database.dto;

import lombok.Data;

@Data
public class HelpDTO {
    private Integer staffId;

    private Integer parentHelpId;

    private String status;

    private String type;

    private Integer shopId;

}
