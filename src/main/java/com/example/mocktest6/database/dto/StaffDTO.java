package com.example.mocktest6.database.dto;

import lombok.Data;



@Data
public class StaffDTO {
    private String staffCode;
    private String staffName;
    private String tel;
    private String status;
    private Integer shopId;
    private Integer helpId;
    private String idNo;
}
