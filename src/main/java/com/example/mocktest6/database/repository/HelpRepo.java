package com.example.mocktest6.database.repository;

import com.example.mocktest6.database.model.Help;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HelpRepo extends JpaRepository<Help,Integer> {

    
    @Query(value = "SELECT u FROM Help u where u.createdUser = ?1")
    List<Help> findByCreatedUser(String createdUser);
    
    @Query(nativeQuery = true, value = "select help_id from help start with help_id = ?1 connect by prior help_id = parent_help_id")
    List<Integer> selectHelpIdLevel(int helpId);
    
    @Query(nativeQuery = true, value = "update help set status = '0' where help_id = ?1")
    void delete(int helpId);
}
