package com.example.mocktest6.database.model;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Data
@Table(name="help")
public class Help {

    @Id
    @Column(name = "help_id")
    private int helpId;

    @Column(name = "help_name")
    private String helpName;

    @Column(name = "parent_help_id")
    private Integer parentHelpId;

    @Column(name = "type")
    private String type;

    @Column(name = "position")
    private String position;

    @Column(name = "content")
    private String content;

    @Column(name = "status")
    private String status;

    @Column(name = "created_user")
    private String createdUser;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "staff_code")
    private Staff staff;

}
