package com.example.mocktest6.database.model;

import jakarta.persistence.*;
import java.util.List;
import lombok.Data;

@Entity
@Data
@Table(name = "staff")
public class Staff {
    @Id
    @Column(name = "staff_id")
    private Integer staffId;

    @Column(name = "staff_code")
    private String staffCode;

    @Column(name = "staff_name")
    private String staffName;

    @Column(name = "tel")
    private String tel;

    @Column(name = "address")
    private String address;

    @Column(name = "shop_id")   
    private Integer shopId;

    @Column(name = "id_no")
    private String idNo;

    @Column(name = "status")
    private String status;
    
    
    @OneToMany(mappedBy = "staff", cascade = CascadeType.ALL)
    private List<Help> helps;


}
